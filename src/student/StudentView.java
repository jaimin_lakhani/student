/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package student;

/**
 *
 * @author jaiminlakhani
 */
public class StudentView {
    public void printStudentDetails(String studentName, String studentRollNo, String studentID) {
        System.out.println("Student: ");
        System.out.println("Name: "+studentName);
        System.out.println("Roll no: "+studentRollNo);
        System.out.println("Student ID: "+studentID);
    }
}
